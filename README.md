# puppet-codes

> To run puppet agent to generate certs or fetch catalogs:
```sh
$ sudo puppet agent --verbose --no-daemonize --onetime
or
$ sudo puppet agent -t
```

> To validate puppet code for syntax errors:
```sh
$﻿ sudo puppet parser validate site.pp
```

> To apply resources on all nodes, write your code in the 'default' node section of site.pp
```sh
$ node default {
  ---
}
```

> To test if the changes apply fine on the agent before actually running the catalog
```sh
$ sudo puppet agent --test
```

> Using Classes (modules/init.pp) in the main manifest(site.pp)
```sh
# This is site.pp file
$ node default {
  class{ 'myclass': } # this is the class name in init.pp
}
```

> **ensure** attribute in package can be present/absent/latest -- latest will make sure latest software is installed

> list all modules
puppet module list

> Searching modules using puppet
puppet module search <string> # nginx/tomcat/...

> Install modules
puppet module install <module_name> --version <version>
puppet module install --modulerepository <repository_location> <module_name> --version <version>
puppet module install ~/module.tar.gz

> Generate module templates
puppet module generate <user_name>-<module_name>
