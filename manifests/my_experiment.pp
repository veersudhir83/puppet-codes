#  To create a directory and place a file with inline_template

node 'test02.edureka.com' {

  $user_name = 'edureka'

  $user_group = 'edureka'

  $create_file = true

  # for debug output on the puppet client - with full source information
  notify { "Running on ${facts['osfamily']}:${facts['fqdn']} agent":
    withpath => true,
  }

  if $facts['osfamily'] == 'Debian' {

    file {'create_motd'
      path     => '/etc/motd',
      ensure   => present,
      mode     => '0644',
      content  => inline_template("You logged in at <%= Time.now %>\n"),
    }

    group { $user_group:
      name   => $user_group,
      ensure => present,
    }

    user { $user_name:
      name     => $user_name,
      ensure   => present,
      groups   => $user_name,
      require  => Group[$user_name],
      shell    => '/bin/bash',
      home     => '/home/$user_name',
    }

    package { 'maven':
      ensure          => present,
      install_options => "--force-yes",
    }

    package { 'ant':
      ensure          => present,
      install_options => "--force-yes",
    }

    if !$create_file {
      # a fuller example, including permissions and ownership
      file { 'test1.dir':
        path     => "/home/$user_name/test1",
        ensure   => absent,
        owner    => $user_name,
        group    => $user_group,
        recurse  => true,
        purge    => true,
        force    => true,
        mode     => '0750',
        require  => File['create_text_file'],
      }

      file { 'create_text_file':
        path     => "/home/$user_name/test1/pup_test.txt",
        ensure   => absent,
        owner    => $user_name,
        group    => $user_group,
        mode     => '0750',
        content  => inline_template("First file is created by puppet at <%= Time.now %>\n"),
      }
    } else {
      # a fuller example, including permissions and ownership
      file { 'test1.dir':
        path     => "/home/$user_name/test1",
        ensure   => 'directory',
        owner    => $user_name,
        group    => $user_group,
        recurse  => true,
        purge    => true,
        force    => true,
        mode     => '0750',
      }

      file { 'create_text_file':
        path     => "/home/$user_name/test1/pup_test.txt",
        ensure   => present,
        owner    => $user_name,
        group    => $user_group,
        mode     => '0750',
        require  => File['test1.dir'],
        content  => inline_template("First file is created by puppet at <%= Time.now %>\n"),
      }
    }
  }
}
