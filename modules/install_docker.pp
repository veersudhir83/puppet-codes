# Class: install_docker
# ===========================
# Installs docker
#
# Authors
# -------
# Sudheer Veeravalli <veersudhir83@gmail.com>
#
# Copyright
# ---------
# Copyright 2017 Sudheer Veeravalli
#
class install_docker {

  package { 'docker.io':
    ensure   => latest,
  }

  service { 'docker':
    ensure   => 'running',
  }

  if (Package['docker.io'] == absent){
    exec { 'installDocker':
      path    => '/bin:/usr/sbin:/usr/bin:/sbin',
      command => 'apt-get install -y docker.io',
      user    => 'root',
    }
  }
}
