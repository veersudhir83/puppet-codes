# Class: java_tomcat
# ===========================
# Installs java and tomcat
#
# Authors
# -------
# Sudheer Veeravalli <veersudhir83@gmail.com>
#
# Copyright
# ---------
# Copyright 2017 Sudheer Veeravalli
#
class java_tomcat {

  if $::osfamily == 'Debian' {
    # Needed for update-java-alternatives
    package { 'java-common':
      ensure => present,
      notify => Exec['installJava'],
    }
  }

  exec { 'installJava':
    path    => '/bin:/usr/sbin:/usr/bin:/sbin',
    command => 'add-apt-repository -y ppa:openjdk-r/ppa && apt-get update && apt-get install -y openjdk-8-jdk',
    user    => 'root',
  }

  group { 'tomcat':
    name   =>'tomcat',
    ensure => present,
  }

  user { 'tomcat':
    name     =>'tomcat',
    ensure   => present,
    groups   => 'tomcat',
    require  => Group['tomcat'],
    notify   => Exec['gettomcat'],
  }

  exec { 'gettomcat':
    path    => '/bin:/usr/sbin:/usr/bin:/sbin',
    command => 'wget http://www-eu.apache.org/dist/tomcat/tomcat-8/v8.0.46/bin/apache-tomcat-8.0.46.tar.gz',
    cwd     => '/tmp',
    require => Exec['installJava'],
  }

  exec { 'extracttomcat':
    path    => '/bin:/usr/sbin:/usr/bin:/sbin',
    cwd     => '/tmp',
    command => 'mkdir -p /opt/tomcat && tar xvf apache-tomcat-8*tar.gz -C /opt/tomcat --strip-components=1',
    require => Exec['gettomcat'],
  }

  exec { 'startTomcat':
    path    => '/bin:/usr/sbin:/usr/bin:/sbin',
    require => [ Exec['extracttomcat'], User['tomcat'] ] ,
    command => 'chgrp -R tomcat /opt/tomcat && /opt/tomcat/bin/startup.sh',
    cwd     =>'/opt/tomcat/bin',
  }
}
