# Class: vim_apache2_motd
# ===========================
# Installs vim, installs apache2 and starts, creates a motd file and index.html file
#
# Authors
# -------
# Sudheer Veeravalli <veersudhir83@gmail.com>
#
# Copyright
# ---------
# Copyright 2017 Sudheer Veeravalli
#
class vim_apache2_motd {

  # for debug output on the puppet client - with full source information
  notify { "Running on $fqdn agent": }

  if $facts['osfamily'] == 'Debian' {

    file { 'create_motd':
      path     => '/etc/motd',
      ensure   => present,
      mode     => '0644',
      content  => inline_template("You logged in at <%= Time.now %>\n"),
    }

    package { 'apache2':
      ensure   => latest,
    }

    service { 'apache2':
      ensure   => 'running',
    }

    file { 'welcome_page':
      path    => '/var/www/html/index.html',
      ensure  => 'present',
      content => inline_template("Welcome to puppet !! Time now is <%= Time.now %>\n"),
    }
  }
}
