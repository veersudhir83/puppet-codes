# Class: ant_maven
# ===========================
# Installs ant, maven
#
# Authors
# -------
# Sudheer Veeravalli <veersudhir83@gmail.com>
#
# Copyright
# ---------
# Copyright 2017 Sudheer Veeravalli
#
class ant_maven {
  package { 'maven':
    ensure          => present,
    install_options => "--force-yes",
  }

  package { 'ant':
    ensure          => present,
    install_options => "--force-yes",
  }
}
